import { Controller, Query, Post, Request, HttpCode, Body, HttpStatus, UseGuards } from '@nestjs/common';
import { ApiTags, ApiResponse, ApiBearerAuth, ApiBadRequestResponse, ApiOperation, ApiProduces, ApiCreatedResponse } from '@nestjs/swagger';
import { Routes } from '../constants';
import { AuthGuard } from '@nestjs/passport';
import { QueryDto, FinishedDto, SuccessResponse } from './sync.dto';
import { SyncService } from './sync.service';
import { Instance } from '../auth/auth.decorator';

@ApiTags(Routes.sync.base)
@Controller(Routes.sync.base)
@UseGuards(AuthGuard('webhook'))
export class SyncController {

	constructor(private syncService: SyncService) {}

	@Post(Routes.sync.callback)
	@ApiOperation({ summary: '— ответ от Tomoru' })
    @ApiResponse({ status: HttpStatus.OK, type: SuccessResponse })
	async callback(
		@Instance() account,
		@Body() data: any
	): Promise<SuccessResponse> {

		console.log(account, data);

		//this.syncService()

		return { status: 'ok' };
	}
}
