import { IsNotEmpty, IsNumber, IsOptional, ValidateNested, IsArray, IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty, ApiResponseProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Schema } from 'mongoose';


export enum ResponseStatus {
	OK = 'ok'
}

export enum EventType {
	DialogFinished = 'dialogFinished',
	CompanyFinished = 'companyFinished'
}

export enum DialogFinishedResults {
	DialogFinished = 'dialingFailed',
	AnswerMachine = 'answerMachine',
	RequestedCallback = 'requestedCallback',
	Success = 'success'
}	

export class Attempts {
	@ApiProperty({ type: String, example: '2021-08-09T18:31:42.201' })	
	finished: string;

	@ApiProperty({ enum: DialogFinishedResults, example: DialogFinishedResults.Success  })
	result: DialogFinishedResults;
}


export class additionalInfoDto {

	@IsNotEmpty()
	@IsNumber()
	@ApiProperty({ type: Number, example: 3453453 })
	entity_id: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, example: 'contact' })
  entity_type: string;

}

export class FinishedDto {
  	@IsNotEmpty()
  	@ApiProperty({ enum: EventType, example: EventType.DialogFinished  })
  	type: EventType;

  	@IsNotEmpty()
  	@IsNumber()
  	@ApiProperty({ type: Number, example: 123 })
  	companyId: number;
  	
  	@IsOptional()
  	@ApiPropertyOptional({ type: String, example: '+79990000000' })	
  	phone?: string;

  	@IsOptional()
  	@ValidateNested()
  	@ApiPropertyOptional({ type: () => additionalInfoDto })	
  	additionalInfo?: additionalInfoDto;

  	@IsOptional()
  	@IsArray()
  	@ValidateNested({ each: true })
  	@ApiPropertyOptional({ type: () => [Attempts] })	
  	attempts?: Attempts[]

}


export class QueryDto {
	@IsNotEmpty()
	@ApiProperty({ type: String, example: 'widgetId' })	
	instance: string;
}


export class SuccessResponse {
  @IsNotEmpty()
  @ApiResponseProperty({ enum: ResponseStatus, example: ResponseStatus.OK  })
  status!: string;
}