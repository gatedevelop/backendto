import { Controller, Get, Post, Patch, Request, Param, Body, Headers, Query, HttpCode } from '@nestjs/common';
import { ApiResponse, ApiBearerAuth, ApiBadRequestResponse, ApiOperation, ApiProduces, ApiCreatedResponse } from '@nestjs/swagger';
import { Routes } from '../constants';
import { AuthGuard } from '@nestjs/passport';
import { AccountService } from './../account/account.service';
import { AuthService } from '../auth/auth.service';
import {AmocrmApiService} from '../amocrm-api/amocrm-api.service';


@Controller()
export class ProxyController {

	constructor(
		private accounts: AccountService,
		private auth: AuthService,
		private api: AmocrmApiService
	) {}

	@Get('contacts')
	async getContacts(@Headers('x-leadlab-botid') botId: string, @Query() params: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		const data = await this.api.getContacts(account, params);
		console.log(data);
		return data;
	}

	@Get('contacts/:id')
	async getContact(@Headers('x-leadlab-botid') botId: string, @Param('id') id: number, @Query() params: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		
		const data =  await this.api.getContact(account, id, params);
		console.log(data);
		return data;
	}

	@Get('leads')
	async getLeads(@Headers('x-leadlab-botid') botId: string, @Query() params: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		
		const data = await this.api.getLeads(account, params);
		console.log(data);
		return data;
	}

	@Get('leads/:id')
	async getLead(@Headers('x-leadlab-botid') botId: string, @Param('id') id: number, @Query() params: any ) {

		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);

		const data = await this.api.getLead(account, id, params);
		console.log(data);
		return data;
	}

	@Get('customers')
	async getCustomers(@Headers('x-leadlab-botid') botId: string, @Query() params: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
	
		const data =  await this.api.getCustomers(account, params);
		console.log(data);
		return data;
	}

	@Get('customers/:id')
	async getCustomer(@Headers('x-leadlab-botid') botId: string, @Param('id') id: number , @Query() params: any) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);

		const data = await this.api.getCustomer(account, id, params);
		console.log(data);
		return data;
	}


	@Patch('customers')
	async patchCustomers(@Headers('X-Leadlab-BotId') botId: string, @Body() data: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		
		console.log('ok');
		return await this.api.patchCustomers(account, data);
		
	}

	@Patch('leads')
	async patchLeads(@Headers('X-Leadlab-BotId') botId: string, @Body() data: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		console.log('ok');
		return await this.api.patchLeads(account, data);
	}

	@Patch('leads/:id')
	async patchLead(@Headers('X-Leadlab-BotId') botId: string, @Param('id') id: number , @Body() data: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		console.log('ok');
		return await this.api.patchLead(account, id, data);
	}

	@Patch('contacts/:id')
	async patchContact(@Headers('X-Leadlab-BotId') botId: string, @Param('id') id: number , @Body() data: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		console.log('ok');
		return await this.api.patchContact(account, id, data);
	}

	@Patch('customers/:id')
	async patchCustomer(@Headers('X-Leadlab-BotId') botId: string, @Param('id') id: number , @Body() data: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		console.log('ok');
		return await this.api.patchCustomer(account, id, data);
	}

	@Patch('customers/statuses')
	async patchCustomersStatuses(@Headers('X-Leadlab-BotId') botId: string, @Body() data: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		console.log('ok');
		return await this.api.patchCustomersStatuses(account, data);
	}

	@Patch('customers/statuses/:id')
	async patchCustomerStatuses(@Headers('X-Leadlab-BotId') botId: string, @Param('id') id: number , @Body() data: any ) {
		const account = await this.auth.checkExpired(
			await this.accounts.getAccountByBotId(botId)
		);
		console.log('ok');
		return await this.api.patchCustomerStatuses(account, id, data);
	}

}
