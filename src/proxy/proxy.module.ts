import { Module } from '@nestjs/common';
import { AccountModule } from '../account/account.module';
import { AmocrmApiModule } from '../amocrm-api/amocrm-api.module';
import { AuthModule } from '../auth/auth.module';
import {ProxyController} from './proxy.controller';

@Module({
  imports: [AmocrmApiModule, AccountModule, AuthModule],
  providers: [],
  controllers: [ProxyController]
})
export class ProxyModule {}
