import {AuthGuard} from '@nestjs/passport';
import {Injectable} from '@nestjs/common';

@Injectable()
export class WebhookGuard extends AuthGuard('webhook') {}
