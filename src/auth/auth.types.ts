export enum TokenType {
	Bearer = 'Bearer'
}

export enum GrantTypes {
	RefreshToken = 'refresh_token',
	AuthorizationCode = 'authorization_code'
}

export interface oAuth2TokensRequest {
	
    client_id: string;
	client_secret: string;
	grant_type: GrantTypes,

	refresh_token?: string;
	code?: string;

	redirect_uri: string;
    
}

export interface oAuth2TokensResponse {
	token_type: TokenType;
	expires_in: number;
	access_token: string;
	refresh_token: string;
}