
export interface JwtPayload {

  /**
   * Адрес аккаунта в amoCRM
   * @type {string}
   */
  iss: string;

  /**	
   * Базовый адрес, который сформирован исходя из значения redirect_uri в интеграции
   * @type {string}
   */
  aud: string;

  /**
   * UUID токена
   * @type {string}
   */
  jti: string;

  /**
   * Timestamp, когда был выдан токен
   * @type {number}
   */
  iat: number;

  /**
   * Timestamp, когда токен начинает действовать
   * @type {number}
   */
  nbf: number;

  /**
   * Timestamp, когда токен будет просрочен
   * @type {number}
   */
  exp: number;

  /**
   * ID аккаунта, из которого сделан запрос
   * @type {number}
   */
  account_id: number;

  /**
   * ID пользователя, из под которого сделан запрос
   * @type {number}
   */
  user_id: number;

  /**
   * UUID интеграции, которая сделала запрос
   * @type {string}
   */
  client_uuid: string;
}