import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import { AccountService } from '../../account/account.service';
import { Account } from '../../account/account.schema';
import {Strategy} from 'passport-accesstoken';

@Injectable()
export class WebhookStrategy extends PassportStrategy(
  Strategy,
  'webhook',
) {
  constructor(private readonly accountService: AccountService) {
    super({tokenField: 'instance'}, async (token, done) => {
    	const account = await this.accountService.getAccountById(token);
  		return (account) ? done(null, account) : done(new UnauthorizedException());
    });
  }
}
