import { Strategy } from 'passport-custom';
import {verify} from 'jsonwebtoken';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtPayload } from './jwt.types';
import { AccountService } from '../../account/account.service';
import {AuthService} from '../auth.service';
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {

   static key = 'jwt';

  constructor(
    private readonly accountService: AccountService,
    private readonly config: ConfigService,
    private readonly authService: AuthService
  ) {
    super();
  }

  async validate(request) {

    // Получаем токен из запроса
    const token = request.headers['x-auth-token'] || null;

    // Проверяем
    const payload = <JwtPayload>verify(token, this.config.get<string>('WIDGET_SECRET'));

    const account = await this.accountService.getAccount(Number(payload.account_id));

    if (!account) throw new Error();
   
  	return await this.authService.checkExpired(account);
    
  }

}
