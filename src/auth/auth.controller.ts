import { Controller, Get, HttpCode, Query, HttpStatus } from '@nestjs/common';
import { ApiTags, ApiResponse, ApiBearerAuth, ApiBadRequestResponse, ApiOperation, ApiProduces, ApiCreatedResponse } from '@nestjs/swagger';
import { InstallWidgetDto, SuccessResponse, BadResponse, ResponseStatus } from './auth.dto';
import { AccountService } from '../account/account.service';
import { Routes } from '../constants';
import { AuthService } from './auth.service';
	
@ApiTags(Routes.auth.base)
@Controller(Routes.auth.base)
export class AuthController {


   	constructor(private authService: AuthService) {}

	/**
   * Обратный хук от amoCRM после установки виджета из маркета
   * @see https://www.amocrm.ru/developers/content/oauth/auth-public-integrations
   */
  @Get(Routes.auth.success)
  @ApiOperation({ summary: '— вебхук, который amoCRM дергает после установки виджета' })
  @ApiResponse({ status: HttpStatus.OK, description: 'Виджет успешно установлен', type: SuccessResponse })
  @ApiBadRequestResponse({ status: HttpStatus.BAD_REQUEST, description: 'Не переданы параметры code или referer', type: BadResponse })
  async activate(@Query() widget: InstallWidgetDto): Promise<SuccessResponse> {

  	const { code, referer } = widget;

  	await this.authService.install(code, referer);

    return { 
    	status: ResponseStatus.OK 
    };
  }


}
