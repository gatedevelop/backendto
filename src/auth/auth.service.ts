import { Injectable, Inject, Scope } from '@nestjs/common';
import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';
import { Account } from '../account/account.schema';
import { oAuth2TokensRequest, oAuth2TokensResponse, GrantTypes } from './auth.types';
import { ConfigService } from '@nestjs/config';
import { AccountService } from '../account/account.service';

@Injectable()
export class AuthService {

	private readonly httpModule: AxiosInstance = axios.create();

	private readonly widgetUuid = this.configService.get<string>('WIDGET_CLIENT_UUID');
	private readonly widgetSecret = this.configService.get<string>('WIDGET_SECRET');
	private readonly widgetRedirectUrl = this.configService.get<string>('WIDGET_REDIRECT_URI');

	constructor(
		private readonly configService: ConfigService,
		private readonly accountService: AccountService
	) {}


	async install(code, referer): Promise<Account> {

		const [ subdomain ] = referer.split('.');

		const account = await this.accountService.createAccount(subdomain);
		const tokens  = await this.getAccessTokens(account, code);
		const { id }  = await this.getAccountData(tokens.access_token);

		account.account_id = id;
		account.refresh_token = tokens.refresh_token;
		account.access_token = tokens.access_token;
		account.expires_in = this.getExpiredIn();

		await account.save();

		console.log(account);

		return account;

	}


	private getExpiredIn(): Date {
		var date = new Date();
	 	date.setDate(date.getDate() + 1);
	 	return date;
	}

	async checkExpired(account): Promise<Account> {
		var now = new Date();
		if (now >= account.expires_in) {
			const tokens = await this.getAccessTokens(account);
			account.refresh_token = tokens.refresh_token;
			account.access_token = tokens.access_token;
			account.expires_in = this.getExpiredIn();
			await account.save();
		}
		return account;

	}

	async getAccessTokens(account: Account, code: string|null = null): Promise<oAuth2TokensResponse> {

		const request = (code) 
			? this.buildGetAccessTokenByCodeParams(code)
			: this.buildGetAccessTokenByRefreshTokenParams(account.refresh_token);

		const url = this.buildUrl(account);

		return await this.httpModule.post(url, request).then((response) => response.data);

	}

	private buildGetAccessTokenByCodeParams(code: string): oAuth2TokensRequest {
		return {
	    	client_id: this.widgetUuid,
			client_secret: this.widgetSecret,
			grant_type: GrantTypes.AuthorizationCode,
			code,
			redirect_uri: this.widgetRedirectUrl
	    }
	}

	private buildGetAccessTokenByRefreshTokenParams(refresh_token: string): oAuth2TokensRequest {
		return {
	    	client_id: this.widgetUuid,
			client_secret: this.widgetSecret,
			grant_type: GrantTypes.RefreshToken,
			refresh_token,
			redirect_uri: this.widgetRedirectUrl
	    }
	}

	private buildUrl(account: Account): string {
		const { subdomain } = account;
		return `https://${subdomain}.amocrm.ru/oauth2/access_token`;
	}


	private async getAccountData(token: string) : Promise<any> {
		return await this.httpModule.request({
			method: 'GET',
			url: `https://www.amocrm.ru/oauth2/account/subdomain`,
			headers: {
			  Authorization: `Bearer ${token}`,
		      Accept: 'application/json',
		      'Content-Type': 'application/json',
		    }
		}).then((response) => response.data).catch(console.error);

	}



	private async buildHeaders(): Promise<Record<string, string>> {

	    return {
	      Accept: 'application/json',
	      'Content-Type': 'application/json',
	    };
	}
}
