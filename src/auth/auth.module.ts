import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { WebhookStrategy } from './strategies/webhook.strategy';
import { ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { AccountModule } from '../account/account.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    AccountModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
    }),
    JwtModule.registerAsync({
      imports: [],
      useFactory: () => {
        return {
          secret: '',
        };
      },
      inject: [],
    }),
  ],
  providers: [AuthService, JwtStrategy, WebhookStrategy, ConfigService],
  exports: [AuthService, JwtStrategy, WebhookStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
