import { IsNotEmpty, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty, ApiResponseProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class InstallWidgetDto {
  
  @IsNotEmpty()
  @ApiProperty({ type: String })
  code!: string;
  
  @IsNotEmpty()
  @ApiProperty({ type: String })
  referer!: string; 

  @IsOptional()
  @ApiPropertyOptional({ type: Boolean })
  @Transform(value => Boolean(value))
  from_widget?: boolean;

}

export enum ResponseStatus {
	OK = 'ok'
}

export class SuccessResponse {
  @IsNotEmpty()
  @ApiResponseProperty({ enum: ResponseStatus, example: ResponseStatus.OK  })
  status!: string
}

export class BadResponse {

	@ApiResponseProperty({ type: Number, example: 400 })
	statusCode!: number;

	@ApiResponseProperty({ type: Array , example: ['referer should not be empty'] })
	message!: string[];

	@ApiResponseProperty({ type: String, example: 'Bad Request' })
	error!: string;
		
}
