import { Injectable } from '@nestjs/common';
import axios, { AxiosInstance, AxiosResponse } from 'axios';
import {AccountDocument} from '../account/account.schema';
import {AmocrmApiService} from '../amocrm-api/amocrm-api.service';
import { stringify } from 'qs';

@Injectable()
export class HooksService {

	private readonly httpModule: AxiosInstance = axios.create();

	constructor() {}

	async createCall(account: AccountDocument, payload): Promise<any> {

		return await this.httpModule({
		  method: 'POST',
		  headers: { 
		  	'content-type': 'application/x-www-form-urlencoded',
		  	'X-AmoCrm-Access-Token': account.access_token
		  },
		  data: stringify(payload),
		  url: `http://persistent-services.backend.tomoru.ru/leadlab/webhook/${ account.botId }`,
		}).then((response: AxiosResponse) => {
			console.log(response);
			return response.data;
		}).catch(console.error);

	}







}
