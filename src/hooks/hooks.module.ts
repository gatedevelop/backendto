import { Module } from '@nestjs/common';
import { AccountModule } from '../account/account.module';
import { AmocrmApiModule } from '../amocrm-api/amocrm-api.module';
import { HooksController } from './hooks.controller';
import { HooksService } from './hooks.service';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [AmocrmApiModule, AccountModule, AuthModule],
  providers: [HooksService],
  controllers: [HooksController]
})
export class HooksModule {}
