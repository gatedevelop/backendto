import { Controller, Get, Post, Request, HttpCode, Body, HttpStatus, UseGuards } from '@nestjs/common';
import { ApiTags, ApiResponse, ApiBearerAuth, ApiBadRequestResponse, ApiOperation, ApiProduces, ApiCreatedResponse } from '@nestjs/swagger';
import { Routes } from '../constants';
import { AuthGuard } from '@nestjs/passport';
import { Instance } from '../auth/auth.decorator';
import { HooksService } from './hooks.service';
import { AccountService } from './../account/account.service';
import { AuthService } from '../auth/auth.service';

@ApiTags(Routes.company.base)
@Controller(Routes.hooks.base)
export class HooksController {

	constructor(
		private accounts: AccountService,
		private auth: AuthService,
		private hooksService: HooksService
	) {}

	@Post(Routes.hooks.dp)
	@ApiOperation({ summary: '— инициализировать звонок' })
	async add(
		@Body() payload: any
	) {

		console.log('hook!');
		const { account_id, event, subdomain } = payload;
		const { data, type } = event;

		console.log(payload);

		let account: any = await this.accounts.getAccount(account_id);

		account = await this.auth.checkExpired(account);

		console.log('ok auth hook!');


		if (['20','21'].includes(type)) {
			const result = await this.hooksService.createCall(account, {
				account: {
					id: account_id,
					subdomain,
				},
				customers: {
					status: [{
						id: data.id,
						status_id: data.status_id,
						old_status_id: data.old_status_id
					}]
				}
			})
		} else {
			const result = await this.hooksService.createCall(account, {
				account: {
					id: account_id,
					subdomain,
				},
				leads: {
					status: [{
						id: data.id,
						status_id: data.status_id,
						pipeline_id: data.pipeline_id,
						old_status_id: data.status_id,
						old_pipeline_id: data.pipeline_id,
					}]
				}
			})
		}



		return { status: 'ok' };

	}




}
