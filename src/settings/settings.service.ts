import { Injectable } from '@nestjs/common';
import { AccountService } from '../account/account.service';
import { AccountDocument, Account } from '../account/account.schema';

@Injectable()
export class SettingsService {


	constructor(private accountService: AccountService) {}

	async getSettings(account: Account) {
		return {
			botId: account.botId,
			apiKey: account.apiKey
		}
	}

	async updateSettings(account: AccountDocument, settings): Promise<AccountDocument> {

		return await this.accountService.updateAccount(account._id, settings);

	}

}
