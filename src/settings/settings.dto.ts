import { IsNotEmpty } from 'class-validator';
import { ApiProperty, ApiResponseProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class SettingsDto {
  
  @ApiProperty({ type: String })
  botId!: string;
  
  @ApiProperty({ type: String })
  apiKey!: string; 

}
