import { Controller, Get, Post, Request, HttpCode, Body, HttpStatus, UseGuards } from '@nestjs/common';
import { ApiTags, ApiResponse, ApiBearerAuth, ApiBadRequestResponse, ApiOperation, ApiProduces, ApiCreatedResponse } from '@nestjs/swagger';
import { SettingsService } from './settings.service';
import { Routes } from '../constants';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { SettingsDto } from './settings.dto';
import { Instance } from '../auth/auth.decorator';


@ApiTags(Routes.settings.base)
@ApiBearerAuth('jwt')
@UseGuards(JwtAuthGuard)
@Controller(Routes.settings.base)
export class SettingsController {

	constructor(private settingsService: SettingsService) {}

	@Get()
	@ApiOperation({ summary: '— получение настроек виджета' })
  	@ApiResponse({ status: HttpStatus.OK, type: SettingsDto })
	async get(
		@Instance() account,
		@Request() req
	) {
		return await this.settingsService.getSettings(account)
	}

	@Post()
	@ApiOperation({ summary: '— обновление настроек виджета' })
    @ApiResponse({ status: HttpStatus.OK, type: SettingsDto })
	async post(
		@Instance() account,
		@Body() settings: SettingsDto
	) {
		return await this.settingsService.updateSettings(account, settings);
	}


}
