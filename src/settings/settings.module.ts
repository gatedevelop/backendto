import { Module } from '@nestjs/common';
import { SettingsService } from './settings.service';
import { SettingsController } from './settings.controller';
import { AccountModule } from '../account/account.module';

@Module({
  imports: [AccountModule],
  providers: [SettingsService],
  controllers: [SettingsController]
})
export class SettingsModule {}
