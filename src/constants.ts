export const ROOT_URL = 'http://localhost:3500';

export const AMOCRM_OPTIONS = 'AMOCRM_OPTIONS';


/* -------- URLs -------- */
export const Routes = {
	auth: {
		base: 'auth',
		success: 'success',
	},
	settings: {
		base: 'settings'
	},
	company: {
		base: 'companies',
		add: 'create'
	},
	hooks: {
		base: 'hooks',	
		dp: 'digital_pipeline'
	},
	sync: {
		base: 'sync',
		callback: 'callback'
	}
}