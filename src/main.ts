import { NestFactory,  } from '@nestjs/core';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

const env = process.env.ENVIRONMENT;

async function bootstrap() {

  const app = await NestFactory.create(AppModule, {
    logger: console
  });

  app.enableCors();

  app.use(function (request, response, next) {
    
    const { url, method } = request;

    console.log(`[${method}] ${url}`);
    
    next();
  });
 
  app.useGlobalPipes(new ValidationPipe({ transform: true, transformOptions: {enableImplicitConversion: true} }));
  app.setGlobalPrefix('api/v1');

  const config = new DocumentBuilder()
    .setTitle('Tomoru + amoCRM')
    .addApiKey({
        type: 'apiKey',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'X-Auth-Token',
        description: 'JWT токен для запросов с frontend-a',
        in: 'header',
    }, 'JWT')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'jwt',
    )
    .addServer('http://localhost:3500', 'Local (development) server')
    .addServer('https://amowidget.tomoru.ru', 'Main (production) server')
    .setDescription('Backend для работы с виджетом Tomoru в amoCRM')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  await app.listen(Number(process.env.PORT));
}
bootstrap();
