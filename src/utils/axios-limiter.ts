import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';
import PQueue from 'p-queue';

export class AxiosRequestLimiter {
  private queue: Record<string, PQueue> = {};
  private axios: AxiosInstance = axios.create();
  private rps = 6;

  constructor() {}

  async add(request: AxiosRequestConfig): Promise<AxiosResponse> {
    const url = new URL(request.url || '');
    const domain = url.hostname;
    this.createQueue(domain);
    return await this.queue[domain].add(() => this.axios.request(request));
  }

  createQueue(domain): void {
    if (!this.queue[domain])
      this.queue[domain] = new PQueue({
        intervalCap: this.rps,
        interval: 1000,
      });
  }

  getInstance(axios): AxiosRequestLimiter {
    this.axios = axios;
    return this;
  }
}

export const axiosQueue = new AxiosRequestLimiter();
