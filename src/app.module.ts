import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AmocrmApiModule } from './amocrm-api/amocrm-api.module';
import { CompaniesModule } from './companies/companies.module';
import { SettingsModule } from './settings/settings.module';
import { HooksModule } from './hooks/hooks.module';
import { ProxyModule } from './proxy/proxy.module';
import { SyncModule } from './sync/sync.module';
import { ConfigService } from '@nestjs/config';

const env = process.env.ENVIRONMENT;

@Module({
  imports: [
   	ConfigModule.forRoot({
   		envFilePath: (env) ? [`./.env_${env}`] : ['./.env'],
   		isGlobal: true
   	}),
    MongooseModule.forRootAsync({
  	  useFactory: async (config: ConfigService) => ({
  	    uri: `mongodb://${ config.get('DB_USER') }:${ config.get('DB_PASSWORD') }@${ config.get('DB_HOST') }:${ config.get('DB_PORT') }/${ config.get('DB_NAME') }`,
  	    useNewUrlParser: true, 
  		  useUnifiedTopology: true 
  	  }),
      inject: [ConfigService],
  	}),
  	AuthModule,
  	CompaniesModule,
  	AmocrmApiModule,
  	SettingsModule,
  	SyncModule,
    ProxyModule,
    HooksModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
