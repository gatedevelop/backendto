import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Exclude } from 'class-transformer';
import { Document } from 'mongoose';

export type AccountDocument = Account & Document;

@Schema()
export class Account {
  /**
   * ID аккаунта amoCRM
   */
  @Prop()
  account_id: number;

  /**
   * Суддомен акканта для запросов к API
   */
  @Prop()
  subdomain: string;

  /**
   * oAuth2 Access Token
   */
  @Prop()
  @Exclude()
  access_token: string;

  /**
   * oAuth2 Refresh Token
   */
  @Prop()
  @Exclude()
  refresh_token: string;

  /**
   * Дата истечения токена
   */
  @Prop()
  @Exclude()
  expires_in: Date;


  /**
   * ID бота в Tomoru
   */
  @Prop()
  botId: string;

  /**
   * Ключи API Tomoru
   */
  @Prop()
  apiKey: string;

  /**
   * Флаг активности виджета в аккаунте amoCRM
   */
  @Prop()
  isActive: boolean = true;
}

export const AccountSchema = SchemaFactory.createForClass(Account);