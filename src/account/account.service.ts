import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Account, AccountDocument } from './account.schema';

@Injectable()
export class AccountService {

	constructor(@InjectModel(Account.name) private accountModel: Model<AccountDocument>) {}

	async getAccount(account_id: number): Promise<AccountDocument> {
		return await this.accountModel.findOne({ account_id });
	}

	async getAccountById(_id: string): Promise<AccountDocument> {
		return await this.accountModel.findOne({ _id });
	}

	async getAccountByBotId(botId: string): Promise<AccountDocument> {
		return await this.accountModel.findOne({ botId });
	}


	async createAccount(subdomain: string): Promise<AccountDocument> {
		// Find or create account
    	let account = await this.accountModel.findOne({ subdomain: subdomain });

    	if (account) {
    		account.delete();
    		return await this.accountModel.create({ subdomain: subdomain }).catch(function (e) {
	    		console.error(e);
	    		throw new Error(e);
	    	})
    	}

    	return (account) ? account : await this.accountModel.create({ subdomain: subdomain }).catch(function (e) {
    		console.error(e);
    		throw new Error(e);
    	});
	}


	async updateAccount(id, { botId, apiKey }: AccountDocument) {

    	const account = await this.accountModel.findById(id);
  
    	if (account) return await account.update({ botId, apiKey });
    	else {
    		throw new Error;
    	}
    	
	}

}
