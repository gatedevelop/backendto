import { Controller, Get, Post, Request, HttpCode, Body, HttpStatus, UseGuards } from '@nestjs/common';
import { ApiTags, ApiResponse, ApiBearerAuth, ApiBadRequestResponse, ApiOperation, ApiProduces, ApiCreatedResponse } from '@nestjs/swagger';
import { Routes } from '../constants';
import { AuthGuard } from '@nestjs/passport';
import { CompaniesDto } from './companies.dto';
import { CompaniesService } from './companies.service';
import { Instance } from '../auth/auth.decorator';

@ApiTags(Routes.company.base)
@ApiBearerAuth('jwt')
@UseGuards(AuthGuard('jwt'))
@Controller(Routes.company.base)
export class CompaniesController {

	constructor(private companiesService: CompaniesService) {}

	@Post(Routes.company.add)
	@ApiOperation({ summary: '— создание новой компании' })
    @ApiResponse({ status: HttpStatus.OK, type: CompaniesDto })
	async add(
		@Instance() account,
		@Request() req,
		@Body() company: CompaniesDto
	) {

		this.companiesService.createCompany(account, company);
		return { status: 'ok' };

	}

}
