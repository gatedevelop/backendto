import { IsNotEmpty, IsEnum, IsArray, IsString, IsNumber,  IsOptional, ValidateNested } from 'class-validator';
import { ApiProperty, ApiResponseProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export enum EntityType {
	Leads = 'leads',
	Contacts = 'contacts',
}

export type Ids = Number[];


export class EntityDto {

  @IsNumber()
  id: number;

  @IsString()
  type: string;

  @IsArray()
  @Type(() => String)
  emails: string[];

  @IsArray()
  @Type(() => String)
  phones: string[];

}

export class CompaniesDto {

  @IsString()
  title: string
  
  @IsNotEmpty()
  @IsEnum(EntityType)
  @ApiProperty({
    description: 'Тип сущности', example: EntityType.Contacts
  })
  type: EntityType;
  
  @IsNotEmpty()
  @IsArray()
  @Type(() => EntityDto)
  @ApiProperty({
    description: 'Список сущностей',
    type: [EntityDto]
  })
  @ValidateNested({ each: true })
  list: EntityDto[]; 

  @IsString()
  @IsOptional()
  filter: string

}

