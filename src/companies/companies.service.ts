import { Injectable } from '@nestjs/common';
import axios, { AxiosInstance, AxiosResponse } from 'axios';
import {AccountDocument} from '../account/account.schema';
import {AmocrmApiService} from '../amocrm-api/amocrm-api.service';
import { CompaniesDto, EntityType } from './companies.dto';

@Injectable()
export class CompaniesService {

	private readonly httpModule: AxiosInstance = axios.create();

	constructor(
		private readonly amocrmApiService: AmocrmApiService
	) {}

	private mapContactsPhones(list) {
		return list.map(({ id, phones, type }) => {
			return (phones[0]) ? {
				phone: phones.shift(),
				entity_id: id,
				entity_type: type
			} : null
		}).filter(entity => entity !== null);
	}

	private async mapLeadsPhones(account: AccountDocument, list) {
		const leadsIds = list.map(({ id }) => id);
		const leads = await this.amocrmApiService.getLeadsByListId(account, leadsIds);

		const contactsIds = leads.map((lead) => lead.main_contact?.id || null ).filter((id) => id !== null);
		const contacts = await this.amocrmApiService.getContactsByListId(account, contactsIds);


		let phones = [];

		contacts.forEach((contact) => {
			if (contact?.custom_fields?.length) contact.custom_fields.forEach(({ code, values}) => {
				if (code && code === 'PHONE') {
					phones.push({
						phone: values.shift().value,
						entity_id: contact.id,
						entity_type: 'contact'
					})
				}
			})
		})


		return phones;

	}

	async createCompany(account: AccountDocument, { title, list, type }: CompaniesDto) {

		try {

			const { companyId } = await this.addCompany(account, title);
			const phones =  (type === EntityType.Leads) 
				? await this.mapLeadsPhones(account, list)
				: this.mapContactsPhones(list);


			await this.addPhones(account, companyId, phones)

			return [];

		} catch(e) { console.error(e); }

		return [];

	}

	private async addCompany(account: AccountDocument, title): Promise<any> {

		console.log(`https://amowidget.tomoru.ru/api/v1/sync/callback?instance=${account._id }`);

		return await this.httpModule
			.post(`https://europe-west1-tomoru-2bb77.cloudfunctions.net/api-callPlanner/${ account.botId }/companies`, {
				title,
				defaultTimezone: 3,
				eventName: 'amoCRM',
				webhookAddress: `https://webhook.site/d0113af1-6436-4266-912d-51368fd82eb0?instance=${account._id }`
			}, {
				headers: {
		          	'Content-Type': 'application/json',
		          	'Authorization': `Bearer ${ account.apiKey }`
		      	}
			}).then((response: AxiosResponse) => {
				return response.data;
			}).catch(console.error);

	}


	private async addPhones(account: AccountDocument, companyId: number, items): Promise<any> {
		return await this.httpModule
			.post(`https://europe-west1-tomoru-2bb77.cloudfunctions.net/api-callPlanner/${ account.botId }/companies/${ companyId }/add-phone-numbers`, items, {
				headers: {
		          	'Content-Type': 'application/json',
		          	'Authorization': `Bearer ${ account.apiKey }`
		      	}
			}).then((response: AxiosResponse) => {
				return response.data;
			}).catch(console.error);

	}







}
