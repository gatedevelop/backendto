import { Module } from '@nestjs/common';
import { CompaniesService } from './companies.service';
import { CompaniesController } from './companies.controller';
import { AmocrmApiModule } from '../amocrm-api/amocrm-api.module';

@Module({
  imports: [AmocrmApiModule],
  providers: [CompaniesService],
  controllers: [CompaniesController]
})
export class CompaniesModule {}
