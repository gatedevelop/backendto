import { Injectable, Inject } from '@nestjs/common';
import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';
import { Account } from '../account/account.schema';
import { ConfigService } from '@nestjs/config';
import { Instance } from '../auth/auth.decorator';
import {axiosQueue} from '../utils/axios-limiter';

@Injectable({})
export class AmocrmApiService {

	private httpService: AxiosInstance = axios.create();
  	private axiosQueue = axiosQueue.getInstance(this.httpService);

	constructor() {} 


	async getContactsByListId(account, ids: number[]): Promise<any[]> {
		let result = [];
		do {
			const pack = await this.getContactsByIds(account, ids.splice(0, 200));
			result = [...result, ...pack];
		} while (ids.length > 0);
		return result;
	}

	private async getContactsByIds(account, ids: number[]): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetContactsByIdsParam(account, ids))
			.then(this.extractItems)
			.catch(() => new Error());
	}


	private mapGetContactsByIdsParam(account, ids: number[]): AxiosRequestConfig {
		return {
			method: 'GET',
			params: {
				id: ids.join(',')
			},
			...this.buildUrl(account, 'api/v2/contacts'),
			...this.buildHeaders(account)
		}
	}


	/**
	 * v2
	 * @param {number[]} ids [description]
	 */
	async getLeadsByListId(account, ids: number[]): Promise<any[]> {
		let result = [];
		do {
			const pack = await this.getLeadsByIds(account, ids.splice(0, 200));
			result = [...result, ...pack];
		} while (ids.length > 0);
		return result;
	}


	private async getLeadsByIds(account, ids: number[]): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetLeadsByIdsParam(account, ids))
			.then(this.extractItems)
			.catch(console.error);
	}

	private mapGetLeadsByIdsParam(account, ids: number[]): AxiosRequestConfig {
		return {
			method: 'GET',
			params: {
				id: ids.join(',')
			},
			...this.buildUrl(account, 'api/v2/leads'),
			...this.buildHeaders(account)
		}
	}

	async getContacts(account, params): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetContacts(account, params))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapGetContacts(account, params): AxiosRequestConfig {
		return {
			method: 'GET',
			params,
			...this.buildUrl(account, 'api/v4/contacts'),
			...this.buildHeaders(account)
		}
	}

	async getLeads(account, params): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetLeads(account, params))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapGetLeads(account, params): AxiosRequestConfig {
		return {
			method: 'GET',
			params,
			...this.buildUrl(account, 'api/v4/leads'),
			...this.buildHeaders(account)
		}
	}

	async getCustomers(account, params): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetCustomers(account, params))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapGetCustomers(account, params): AxiosRequestConfig {
		return {
			method: 'GET',
			params,
			...this.buildUrl(account, 'api/v4/customers'),
			...this.buildHeaders(account)
		}
	}

	async getCustomer(account, id, params): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetCustomer(account, id, params))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapGetCustomer(account, id, params): AxiosRequestConfig {
		return {
			method: 'GET',
			params,
			...this.buildUrl(account, `api/v4/customers/${id}`),
			...this.buildHeaders(account)
		}
	}

	async getLead(account, id, params): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetLead(account, id, params))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapGetLead(account, id, params): AxiosRequestConfig {
		return {
			method: 'GET',
			params,
			...this.buildUrl(account, `api/v4/leads/${id}`),
			...this.buildHeaders(account)
		}
	}
	
	async getContact(account, id, params): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapGetContact(account, id, params))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapGetContact(account, id, params): AxiosRequestConfig {
		return {
			method: 'GET',
			params,
			...this.buildUrl(account, `api/v4/contacts/${id}`),
			...this.buildHeaders(account)
		}
	}


	async patchCustomers(account, data): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapPatchCustomers(account, data))
			.then((response) => response.data)
			.catch(console.error);
	}

	async patchCustomer(account, id, data): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapPatchCustomer(account, id, data))
			.then((response) => response.data)
			.catch(console.error);
	}

	async patchLead(account, id, data): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapPatchLead(account, id, data))
			.then((response) => response.data)
			.catch(console.error);
	}

	async patchContact(account, id, data): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapPatchContact(account, id, data))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapPatchCustomer(account, id, data): AxiosRequestConfig {
		return {
			method: 'PATCH',
			data,
			...this.buildUrl(account, `api/v4/customers/${id}`),
			...this.buildHeaders(account)
		}
	}

	private mapPatchLead(account, id, data): AxiosRequestConfig {
		return {
			method: 'PATCH',
			data,
			...this.buildUrl(account, `api/v4/leads/${id}`),
			...this.buildHeaders(account)
		}
	}

	private mapPatchContact(account, id, data): AxiosRequestConfig {
		return {
			method: 'PATCH',
			data,
			...this.buildUrl(account, `api/v4/contacts/${id}`),
			...this.buildHeaders(account)
		}
	}

	private mapPatchCustomers(account, data): AxiosRequestConfig {
		return {
			method: 'PATCH',
			data,
			...this.buildUrl(account, 'api/v4/customers'),
			...this.buildHeaders(account)
		}
	}

	async patchCustomersStatuses(account, data): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapPatchCustomersStatuses(account, data))
			.then((response) => response.data)
			.catch(console.error);
	}
	

	async patchCustomerStatuses(account, id, data): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapPatchCustomerStatuses(account, id, data))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapPatchCustomerStatuses(account, id, data): AxiosRequestConfig {
		return {
			method: 'PATCH',
			data,
			...this.buildUrl(account, 'api/v4/customers/statuses/' + id),
			...this.buildHeaders(account)
		}
	}

	private mapPatchCustomersStatuses(account, data): AxiosRequestConfig {
		return {
			method: 'PATCH',
			data,
			...this.buildUrl(account, 'api/v4/customers/statuses'),
			...this.buildHeaders(account)
		}
	}

	async patchLeads(account, data): Promise<any[]> {
		return this.axiosQueue
			.add(this.mapPatchLeads(account, data))
			.then((response) => response.data)
			.catch(console.error);
	}

	private mapPatchLeads(account, data): AxiosRequestConfig {
		return {
			method: 'PATCH',
			data,
			...this.buildUrl(account, 'api/v4/leads'),
			...this.buildHeaders(account)
		}
	}




	private extractItems(response: AxiosResponse) {
		return response.data._embedded.items;
	}

	private buildUrl(account: Account, path: string): Record<string, string> {
		return {
			url: `https://${account.subdomain}.amocrm.ru/${path}`
		};
	}

	private buildHeaders(account: Account) {
		return {
			headers: {
				Authorization: `Bearer ${account.access_token}`
			}
		}
	}
	

}
