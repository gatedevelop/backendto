import { Module, DynamicModule } from '@nestjs/common';
import { AmocrmApiService } from './amocrm-api.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule],
  providers: [AmocrmApiService],
  exports: [AmocrmApiService],
})
export class AmocrmApiModule {}