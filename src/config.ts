export enum Environment {
  Dev = '',
  Stage = 'stage',
  Production = 'production'
}


// В каком окружении запускается код
export const isDev = process.env.ENVIRONMENT === undefined;
export const isStage = process.env.ENVIRONMENT === Environment.Stage;
export const isProduction = process.env.ENVIRONMENT === Environment.Production;


