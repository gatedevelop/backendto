const express = require('express');
const secrets = require('./../secrets')
const Events = require('./../configs/events');
const router  = express.Router();
const axios = require('axios');
const qs = require('qs');

// Load Models
const Partner = require('./../models/core/partner');
const Widget  = require('./../models/core/widget');
const Account = require('./../models/core/account');
const moment = require('moment');
moment.locale('ru');

var options = {
  apiVersion: 'v1', // default
  endpoint: 'http://164.92.233.98:8200', // default
  token: 's.40DIOEBdWqu6i16GPFNZdswZ' // optional client token; can be fetched after valid initialization of the server
};

// get new instance of the client
var vault = require("node-vault")(options);
vault.auths();

const notify = async (message, widget) => {

	await axios.post('https://hooks.slack.com/services/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', {
		text: message + `\n\nWidget ID: ${widget._id} \nАккаунт ID: <https://${widget.account.subdomain}.amocrm.ru/|${widget.account.account}> \n\n@iamkuper`
	})
}

router.get('/list', async (req, res, next) => {

	let accounts = await Widget.find({}).populate('account');

	res.json(accounts);

});

router.get('/partners', async (req, res, next) => {

	let partners = await Partner.find({});
	res.json(partners);

});

router.post('/partners', async (req, res, next) => {

	let partner = new Partner(req.body);

	partner.save();

	res.json(partner);

});

router.post('/partners/:id/link/:widget', async (req, res, next) => {


	let widget  = await Widget.findOne({ _id: req.params.widget });
	let partner = await Partner.findOne({ _id: req.params.id });

	widget.partner = partner._id;
	await widget.save();

	res.json({
		widget: widget,
		partner: partner
	});

});

router.get('/widget/:widget', async (req, res, next) => {


	let widget  = await Widget.findOne({ _id: req.params.widget }).populate('partner');

	res.json(widget);

});



router.get('/:widget/get', async (req, res, next) => {


	let widget  = await Widget.findOne({ _id: req.params.widget }).populate('account');
	
	res.json(widget);

});


router.get('/autologin/:subdomain', async (req, res, next) => {

	let subdomain = req.params.subdomain;

	let date = new Date();
	date.setDate(date.getDate() - 1);




	let account  = await Account.findOne({ subdomain: subdomain });
	//if (!account) return res.json({ error: 404 });
	let widgets  = await Widget.findOne({ account: account._id }).populate('account');

	
	res.json(widgets);

});


router.get('/installed/:subdomain', async (req, res, next) => {

	let subdomain = req.params.subdomain;

	let date = new Date();
	date.setDate(date.getDate() - 1);

	let account  = await Account.findOne({ subdomain: subdomain });
	let widgets  = await Widget.find({ account: account._id }).populate('account');

	
	res.json(widgets);

});








router.get('/:id/account/send', async (req, res, next) => {

	let widget = await Widget.findOne({ _id: req.params.id }).populate('account');
	let instance = secrets[widget.uuid];

	axios.get(`https://${widget.account.subdomain}.amocrm.${widget.account.top_level_domain}/settings/widgets/`, {
      	headers: {
          	'Authorization': `Bearer ${ widget.access_token }`
      	}
    }).then(async (data) => {

    	let user =  JSON.parse( data.data.match(/\(\'user\'\,(.*?)\)\;/)[1] );
    	let account = JSON.parse(data.data.match(/\(\'account\'\,(.*?})\)\;/)[1]);

    		let users = await axios.get(`https://${account.subdomain}.amocrm.${account.top_level_domain}/api/v2/account?with=users`, {
			      	headers: {
			          	'Authorization': `Bearer ${ widget.access_token }`
			      	}
			    });

			    users = users.data._embedded.users;

			    let api = await axios.get(`https://${account.subdomain}.amocrm.${account.top_level_domain}/api/v4/account`, {
			      	headers: {
			          	'Authorization': `Bearer ${ widget.access_token }`
			      	}
			    });


    	try	{
    		axios.post('https://leadlab.ru/amolab/widgets.php', {
    			account: account,
    			user: user,
    			users: users,
		    	api: api.data,
    			widget: instance
    		}).then((result) => {});

    	} catch (e) {}


    	res.send({
    		user: user,
    		account: account
    	});
    });


});


router.get('/:id/account', async (req, res, next) => {

	let widget = await Widget.findOne({ _id: req.params.id }).populate('account');

	axios.get(`https://${widget.account.subdomain}.amocrm.${widget.account.top_level_domain}/settings/widgets/`, {
      	headers: {
          	'Authorization': `Bearer ${ widget.access_token }`
      	}
    }).then((data) => {


    	console.log(data.data.match(/\(\'account\'\,(.*?})\)\;/)[1]);

    	let user =  JSON.parse(data.data.match(/\(\'user\'\,(.*?)\)\;/)[1] );
    	let account = JSON.parse(data.data.match(/\(\'account\'\,(.*?})\)\;/)[1]);

    	res.send({
    		user: user,
    		account: account
    	});
    });


});


router.get('/:id/refresh', async (req, res, next) => {

	let widget = await Widget.findOne({ _id: req.params.id }).populate('account');

	let responce = await axios.post(`https://${ widget.account.subdomain }.amocrm.${ widget.account.top_level_domain }/oauth2/access_token`, JSON.stringify({ 
		client_id: widget.uuid,
		client_secret: secrets[widget.uuid].secret,
		grant_type: 'refresh_token',
		refresh_token: widget.refresh_token,
		redirect_uri: `https://amgate.ru/oauth/${ widget.uuid }/success`
	}), {
      	headers: { 'Content-Type': 'application/json' }
    });

    let tokens = responce.data;

    widget.refresh_token = tokens.refresh_token;
    widget.access_token = tokens.access_token;
    widget.expires_in = Math.floor(Date.now() / 1000) + parseInt(tokens.expires_in);
    await widget.save();

    res.send(tokens);

});


router.get('/:id/payment/year', async (req, res, next) => {

	let widget = await Widget.findOne({ _id: req.params.id }).populate('account');


	let now = Math.floor(Date.now() / 1000);

    widget.payment_to = (now > widget.payment_to) ? now + 60*60*24*365 : widget.payment_to  + 60*60*24*365;
    await widget.save();

    notify(`Виджет ${secrets[widget.uuid].name} продлем на 1 год.`, widget);

    res.send(widget);

});


router.get('/:id/payment/half', async (req, res, next) => {

	let widget = await Widget.findOne({ _id: req.params.id }).populate('account');


	let now = Math.floor(Date.now() / 1000);

	let half = 60*60*24*365 / 2;

    widget.payment_to = (now > widget.payment_to) ? now + half : widget.payment_to  + half;
    await widget.save();

    notify(`Виджет ${secrets[widget.uuid].name} продлем на 6 месяцев.`, widget);

    res.send(widget);

});

router.get('/:id/payment/3day', async (req, res, next) => {

	let widget = await Widget.findOne({ _id: req.params.id }).populate('account');


	let now = Math.floor(Date.now() / 1000);

	let half = 60*60*24*3;

    widget.payment_to = (now > widget.payment_to) ? now + half : widget.payment_to  + half;
    await widget.save();

    notify(`Виджет ${secrets[widget.uuid].name} продлем на 3 дня.`, widget);

    res.send(widget);

});



router.post('/:id/push', async (req, res, next) => {

	let widget = await Widget.findOne({ _id: req.params.id }).populate('account');

	let link = req.body.link || false;

	await widget.sendPush(req.body.header, req.body.message, link);

	return res.send('OK');

})

router.get('/count', async (req, res, next) => {

	let offset = Number(req.params.page)*100 - 100 || 0;
	let widgets = await Widget.count({ 
		expires_in: { 
			$gte: Math.floor(Date.now() / 1000) 
		} 
	});



	
	return res.send({ widgets });

})

router.get('/notify', async (req, res, next) => {

	let offset = Number(req.params.page)*100 - 100 || 0;
	let widgets = await Widget.find({ 
		expires_in: { 
			$gte: Math.floor(Date.now() / 1000) 
		} 
	}).sort( '-updated_at' )
	.limit( 1000 )
	.select('expires_in access_token')
	.populate('account');

	for (var i = widgets.length - 1; i >= 0; i--) {

		let widget = widgets[i];
		let link = 'https://t.me/amgate';

		try {

			let html = await axios.get(`https://${widget.account.subdomain}.amocrm.${widget.account.top_level_domain}/settings/widgets/`, {
		      	headers: {
		          	'Authorization': `Bearer ${ widget.access_token }`
		      	}
		    })
			let account = JSON.parse(html.data.match(/\(\'account\'\,(.*?)\)\;/)[1]);

			if (account.pay_type === 'paid') {
				let paid_till = moment.unix(account.paid_till);
				let paid_from = moment.unix(account.paid_from);

				let days = paid_till.diff(moment(), 'days');

				if (days < 30) {
					console.log(`Подписка amoCRM закончиться через ${days} дней - получите до 7 месяцев в подарок при оплате!`);
					await widget.sendPush('Gate', `Подписка amoCRM закончиться через ${days} дней - получите до 7 месяцев в подарок при оплате!`, link);
				}


			} else if (account.pay_type === 'blocked') {

				console.log(`Подписка amoCRM окончена - получите до 7 месяцев в подарок при оплате!`);
				await widget.sendPush('Gate', `Подписка amoCRM окончена - получите до 7 месяцев в подарок при оплате!`, link);

			} else {
				console.log(`Демо-период amoCRM скоро окончится - получите до 7 месяцев в подарок при оплате!`);
				await widget.sendPush('Gate', `Демо-период amoCRM скоро окончится - получите до 7 месяцев в подарок при оплате!`, link);
			}



		} catch (e) {}



	





	}


	
	return res.send({ widgets });

})


router.get('/all/:page', async (req, res, next) => {

	let offset = Number(req.params.page)*100 - 100 || 0;
	let widgets = await Widget.find({ 
		expires_in: { 
			$lte: Math.floor(Date.now() / 1000) 
		} 
	}).limit( 10000 )
    .sort( 'updated_at' )
	.select('expires_in');


	for (var i = widgets.length - 1; i >= 0; i--) {
		await Widget.findOne({ _id: widgets[i]._id }).populate('account');
	}

	
	return res.send(widgets);

})


// [POST] /success
router.get('/:uuid/success', async (req, res, next) => {

	let code = req.query.code || null;
	let referer = req.query.referer || null;
	let state = req.query.state || null;
	let client_id = req.params.uuid || null;
	let instance = secrets[req.params.uuid];


	if (state) res.redirect(301, `https://${referer}/settings/widgets/#${instance.code}`);
	else res.json({ status: 'install', widget: client_id, referer: referer });

	try	{

		// oAuth
		let data = await Widget.oAuth(referer, {
			client_id: client_id,
			client_secret: instance.secret,
			code: code,
			widget: client_id
		});

		 

		// Create or Update account
	    let account = await Account.findOneAndUpdate({ account: data.account.id }, {
	    	account: parseInt(data.account.id),
	    	subdomain: data.account.subdomain,
	    	top_level_domain: data.account.top_level_domain,
	    	language: (data.account.top_level_domain === 'ru') ? 'ru' : 'en'
	    }, { new: true, upsert: true });


	    // Create or Update widget
	    let widget = await Widget.findOneAndUpdate({ account: account._id, uuid: instance.uuid }, {
	    	account: account._id, 
	    	uuid: instance.uuid,
	    	access_token: data.tokens.access_token,
	    	refresh_token: data.tokens.refresh_token,
	    	expires_in: Math.floor(Date.now() / 1000) + data.tokens.expires_in
	    }, { new: true, upsert: true });

	    console.log(widget);


	    let _new = false;

	    if (widget.settings === null) {
	    	_new = true;
	    	widget.settings = instance.defaults;
	    	widget.markModified('settings');
	    	await widget.save();
	    }

	    widget.account = account;


	    try {
	    	let code = instance.code.replace('amgate_', '');
			await vault.write(`amocrm/data/${ code }/${ account.account }`, { 
				data: { 
					ACCESS_TOKEN: widget.access_token,
					REFRESH_TOKEN: widget.refresh_token,
					EXPIRES_AT: String(widget.expires_in)
				}
			});
	    } catch (vaultError) {} 
	   

	    Events.emit(`${ instance.uuid }/install`, widget, null);

	    try	{
	    	
	    	axios.get(`https://${account.subdomain}.amocrm.${account.top_level_domain}/settings/widgets/`, {
		      	headers: {
		          	'Authorization': `Bearer ${ widget.access_token }`
		      	}
		    }).then(async (data) => {

		    	let user =  JSON.parse( data.data.match(/\(\'user\'\,(.*?)\)\;/)[1] );
		    	let account = JSON.parse(data.data.match(/\(\'account\'\,(.*?)\)\;/)[1]);


		    	let users = await axios.get(`https://${account.subdomain}.amocrm.${account.top_level_domain}/api/v2/account?with=users`, {
			      	headers: {
			          	'Authorization': `Bearer ${ widget.access_token }`
			      	}
			    });

			    users = users.data._embedded.users;

			    let api = await axios.get(`https://${account.subdomain}.amocrm.${account.top_level_domain}/api/v4/account`, {
			      	headers: {
			          	'Authorization': `Bearer ${ widget.access_token }`
			      	}
			    });


			    instance._id = widget._id;

		    	try	{
		    		if (_new) {


		    			notify(`Виджет ${instance.name} установлен из маркета.`, widget);

		    			widget.sendPush(instance.name, 'Спасибо за установку! Нужна помощь в настройке?');
		    			axios.post('https://leadlab.ru/amolab/widgets.php', {
			    			account: account,
			    			users: users,
			    			api: api.data,
			    			user: user,
			    			widget: instance
			    		}).then((result) => {});
		    		}

		    	} catch (e) {}
		    		

		    });
		    	
		    	
	    	
	    } catch (e) {
	    	console.error(e);
	    }


	} catch (e) {
		// return next(new Error('oAuth error'));
	}

});


module.exports = router;